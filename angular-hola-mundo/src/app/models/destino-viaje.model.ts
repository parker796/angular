//estas son clases planas llamadas en typescript lo que hicimos fue crear un arreglo en base a la entrada que me den
//en los inputs sin generarlas como el tipo de dato que definiriamos estos es de nuestro dominio de negocios
export class destinoviaje{
    //creamos una variable para ver si esta seleccionado
    private selected: boolean;
    public servicios: string[]; //algo fictisio
	id: any;
  /*  nombre: string;
    imagenurl: string;

    constructor( n: string, i: string){
        this.nombre = n;
        this.imagenurl = i;
    }*/
    //esto igual hace a lo mismo que hicimos arriba
    constructor(public nombre: string, public imagenurl: string) { 
        this.servicios = ['pileta', 'desayuno']; //estos son como servicios de destinos
    }
    //creamos tanto su setter como su getter para encapsular su contenido 
    isSelected(): boolean {
        return this.selected;
}
    setSelected(s: boolean) {
    this.selected = s;
    }    
}
