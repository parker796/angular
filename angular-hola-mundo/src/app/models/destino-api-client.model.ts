
import { destinoviaje } from './destino-viaje.model'; //importamos el modelo destino viaje
import { Subject, BehaviorSubject } from 'rxjs';
//aqui ya integramos redux que es progrmacion reactiva para los observables

//export class es para poner el nombre de mi clase 
export class DestinosApiClient {
	destinos: destinoviaje[];
	//este es nuestro observable favorito que es un flujo en tiempo real y se propagar en la aplicacion
	//es un observable que recuerda el valor actual y puede tener muchos suscriptores BehaviorSubject
	/*Correcto, a diferencia de otros observables, este siempre notifica a sus nuevos suscriptores el último evento ocurrido.*/
	current: Subject<destinoviaje> = new BehaviorSubject<destinoviaje>(null); //usamos el uso de programacion reactiva rxjs
	constructor() {
       this.destinos = []; //lo inicializamos en nulo
	}
	add(d:destinoviaje){ //esto agrega los destino tanto el nombre como elk destino
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos; //retorna los destinos agregado
	}
	getById( id: String): destinoviaje {
		return this.destinos.filter(function(d) { return d.id.toString() === id })[0];
	}
	elegir(d: destinoviaje) {
		this.destinos.forEach(x => x.setSelected(false)); //generamos una lambda
		d.setSelected(true);
		this.current.next(d); //aqui le propagamos el evento observable que fue el destino selecccionado
	}
	subscribeOnChange(fn) {//susbcscribimos las actualizaciones del observable current
		this.current.subscribe(fn); //puentamos la subscripcion del destino
	}
}