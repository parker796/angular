import { Component, OnInit, Output, EventEmitter } from '@angular/core';
//importamos nuestra clase de typescript para instanciar la clase asi cargamos nuestros modelos en typescript
import { destinoviaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destino-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  //esto era de la otra parte
 // destinos: string[]; //generamos un arreglo
 // destinos: destinoviaje[]; //este va ser nuestro nuevo arreglo de tipo objeto
 /* constructor() { //esta parte la vamos a iterar pero pasando cada cadena a la otra cadena del componente destino
   // this.destinos = ['sinaloa', 'cancun', 'puerto vallarta', 'mazatlan', 'veracruz' ];
    this.destinos = []; //este es un array vacio de tipo objeto
  }*/
  @Output() onItemAdded:EventEmitter<destinoviaje>;
  //destinos: DestinoViaje[];
  updates: string[];

  constructor(public destinosApiClient:DestinosApiClient) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: destinoviaje) => {
      if (d != null) {
        this.updates.push("se a elegido a " + d.nombre);
      }
    })
  }
  ngOnInit(): void {
  }

   /*
  guardar(nombre:string, url:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    //console.log(new DestinoViaje(nombre,url));
    //console.log(this.destinos);
    return false;
  }*/
  agregado(d: destinoviaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: destinoviaje){
    //desmarcar todos los demas en en array de elegidos
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //se marca el elegido
    //d.setSelected(true);
   // this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);
    this.destinosApiClient.elegir(e); //le pasamos la responsabilidad que teniamos al destino-api-client
  }


  /*
//guardar
  agregado(nombre: string, url: string): boolean {
    //console.log(nombre); //esto es de manera tradicional
   // console.log(url);
    //ahora lo hacemos creando nuestro objeto dentro de nuestra carpeta de modelos de negocio
    //console.log(new destinoviaje(nombre, url));
    this.destinos.push(new destinoviaje(nombre, url)); //le vamos a ingresar al array los objetos que traemos de los inputs
   // console.log(this.destinos); //imprimimos lo que se almacena en el array
    return false; //esto es porque como hacemos un submiteo eso hace que recargue la pagina pero nosotros
    //no queremos eso porque estamos desarrollando aplicacion de paginas unicas SPA
  }

  elegido(d: destinoviaje) {
    this.destinos.forEach(function (r) { r.setSelected(false); //aqui desmarcamos como elegido a los demas
    });
    d.setSelected(true); //marcamos como elegi al nuestro cada que se dispare el evento

  }*/
}
