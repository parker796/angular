import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/*Nuestro decorador @NgModule tiene cuatro claves: declaraciones, importaciones, proveedores y
bootstrap*/
//manejando el modulo de ruteo
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { Route } from '@angular/compiler/src/core';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component'; //agregamos los imports para el modulo de formularios
import { DestinosApiClient } from './models/destino-api-client.model';
//estas son las rutas de nuestro proyecto a utilizar 
const routes: Routes = [
  { path: ' ', redirectTo: 'home', pathMatch: 'full' }, //aqui full es por si esta vacio
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino', component: DestinoDetalleComponent } //este un componente que se creo ng generate component 
];

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,   //esto es para el manejo de formularios
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes), //ahora si le pasamos las rutas que tenemos a los imports y modulos del proyecto para poderlos utiliar
  ],
  providers: [
    DestinosApiClient //aqui hacemos una inyeccion de dependencias llamada con el patron singleton
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
