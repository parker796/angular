import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { destinoviaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<destinoviaje>;
  fg: FormGroup; //creamos un formgroup
  minLongitud = 7; //generamos una constante
  searchResult: String[]; //generamos un arreglo de cadenas para las sugerencias

  constructor(private fb: FormBuilder) {  //contruimos el formgroup formbuilder
    //inicializar
    this.onItemAdded = new EventEmitter();
    //vinculacion con tag html
    this.fg = this.fb.group({
     // nombre: ['', Validators.required], //valida de que el formulario no debe estar vacio en angular por defecto
      nombre: ['', Validators.compose([
        Validators.required, //el que teniamos
        //this.NombreValidator, //ys hacemos uso de nuestra funcion
        this.NombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });
    
    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
  }

  ngOnInit(): void {
    //el arreglo de las sugerencias
    let elemenNombre = <HTMLInputElement>document.getElementById("nombre"); //obtenemos el elemnto nombre del input y lo guardamos en una variable igual se puede hacer con el framework de rxjs
    //suscribirnos a cuando nos apretan una clase detectamos el evento
    fromEvent(elemenNombre, 'input') //nososotros estamos escuchando cada que se detecta el input esto es un observable de eventos
      //hacemos una tuberia o un flujo en operaciones en serie de la entrada
      .pipe( //cada eventos de teclado tiene un target nos interesa todo el valor de esa entrada en html
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2), //el texto sigue su secuencia si fue mayor al dos se cumple la condicion se filta se sigue en la operacion si cumple
        debounceTime(200), //milisegundo se espera si nos teclean mas teclas 
        distinctUntilChanged(), //esta parte me frena hasta que se complete los dos milisegundos
        //aqui concatenariamos un webservices y todo seria asincronico y se veria actualizado en nuestro searchResutl
        switchMap(() => ajax('/assets/datos.json')) //como si estuvieramos consultando un webservices lo manda a un archivo estatico
      ).subscribe(AjaxResponse => { //nos suscribimos al cambio que se de correctamente en el archivo json que hicimos
        console.log(AjaxResponse);
        console.log(AjaxResponse.response);
        this.searchResult = AjaxResponse.response;
      });

  }

  guardar(nombre: string, url: string): boolean {
    const d = new destinoviaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
//key:value json
  NombreValidator(control: FormControl): { [s: string]: boolean } /*esta parte indicamos que nos devuelve */{
    const l = control.value.toString().trim().length; //sacamos la longitud de la cadena
    if (l > 0 && l < 5) {
      return { invalidNombre: true } //lo devolvemos a objeto json 
    }
    return null;
  }
  NombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true } //lo devolvemos a objeto json 
      }
      return null;
    }
  }
}
