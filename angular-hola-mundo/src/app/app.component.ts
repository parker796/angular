import { Component } from '@angular/core';
import { Observable, observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-hola-mundo';
  //time esta definido como nuestro observable que es asincrono nuestro objeto el cual esta dado por el flujo de datos en el
  //y para ello necesitamos un observador que va estar mandando los cambios a ese flujo de datos de manera asincrona
  time = new Observable(observer=> {
    setInterval(() => observer.next(new Date().toString()), 1000); //se llama cada un segundo pero contiene un 
    //callback que es el observador se le pasa el siguiente dato pero este es de un string el observador
    //tiene usos para fines como gps, un servidor o algo para ver que pasa en cada cierto tiempo
  });
}
