import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { destinoviaje } from './../models/destino-viaje.model';
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  //como la vamos a instanciar en la otra parte con un parametro por defecto se pasa input y ya no se define
  //nada el constructor
 // @Input() nombre: string; //generamos una variable en typescript la otra variable input recibe el parametro del otro componente que es lista de destinos
  @Input() destino: destinoviaje; //aqui ya no se guarda como un arreglo porque ya le estamos pasando los valores que tiene ese objeto arreglo
  @Input("idx") position: number;
  //vamos a crear @hostbinding tenemos una vinculacion directa de un atributo tag en cuestion con el cual estamos envovieldo el contenido del componente
  //esto es porque definimos un row a fuera y esta clase estaba dentro del componente pero lo ponia abajo
  @HostBinding('attr.class') cssClass = "col-md-4"; //este lo pone como en columnas no una abajo de otra como teniamos el problema
  @Output() clicked: EventEmitter<destinoviaje>; //esto es para hacer referencia a la clase que usamos
  //no olvidemos generar una directiva de lo que hace que este caso es una salida
  constructor() {
    //tenemos que inicalizar este evento para que funcione en el componente
    this.clicked = new EventEmitter(); //de esta forma ya tenemos un evento inicalizado cada vez que se llame
   // this.nombre = "nombre por defecto";
  }
  

  ngOnInit(): void {
  }
//este clase nos encpasula nuestro objeto o wrapea como se dice ahora emitimos nuestro evento 
  ir() {
    //emitimos nuestro evento
    this.clicked.emit(this.destino);
    return false; //no olvidemos esto para que no se recargue la pagina
  }
  
}
